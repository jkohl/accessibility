#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..3"

failed=0
step=1

mkdir reports
docker build -t gitlab-accessibility .

desc="Verify Pa11y installed"
docker run gitlab-accessibility /node_modules/.bin/pa11y

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

desc="Verify Pa11y-CI installed"
docker run gitlab-accessibility /node_modules/.bin/pa11y-ci

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

desc="Verify gl-ally.sh script produces artifacts with correct names"
a11y_urls="https://google.com http://pa11y.org about.gitlab.com about.gitlab.com/users/sign_in"
docker run gitlab-accessibility /bin/bash -c "(
    ./gitlab-accessibility.sh $a11y_urls

	test -f reports/google.com-accessibility.html \
	  -a -f reports/pa11y.org-accessibility.html \
	  -a -f reports/about.gitlab.com-accessibility.html \
	  -a -f reports/about.gitlab.com-users-sign_in-accessibility.html
  )"

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

desc="Produce sample HTML report artifact"
a11y_urls="about.gitlab.com/users/sign_in"
docker run gitlab-accessibility /bin/bash -c "(
  ./gitlab-accessibility.sh $a11y_urls
  cat reports/about.gitlab.com-users-sign_in-accessibility.html
)" > reports/about.gitlab.com-users-sign_in-accessibility.html

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

desc="Produce sample JSON report artifact"
a11y_urls="about.gitlab.com pa11y.org"
docker run gitlab-accessibility /bin/bash -c "(
  ./gitlab-accessibility.sh $a11y_urls
  cat reports/gl-accessibility.json
)" > reports/gl-accessibility.json

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
