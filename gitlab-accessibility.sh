mkdir -p reports

/node_modules/.bin/pa11y-ci -j --config "/pa11y-configs/.pa11yci" $@ > reports/gl-accessibility.json

for url in $@
do
	filename="reports/$(echo $url | sed -E 's/^https?:\/\///' | sed -E 's/\//-/g')-accessibility.html"
	/node_modules/.bin/pa11y --config "/pa11y-configs/pa11y.json" $url | tee $filename > /dev/null
done
